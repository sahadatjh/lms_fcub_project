<!doctype html>
<html class="no-js" lang="">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>FCUB | Dashboard</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="{{asset('backend')}}/img/favicon.png">
    <!-- Normalize CSS -->
    <link rel="stylesheet" href="{{asset('backend')}}/css/normalize.css">
    <!-- Main CSS -->
    <link rel="stylesheet" href="{{asset('backend')}}/css/main.css">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{asset('backend')}}/css/bootstrap.min.css">
    <!-- Fontawesome CSS -->
    <link rel="stylesheet" href="{{asset('backend')}}/css/all.min.css">
    <!-- Flaticon CSS -->
    <link rel="stylesheet" href="{{asset('backend')}}/fonts/flaticon.css">
    <!-- Full Calender CSS -->
    <link rel="stylesheet" href="{{asset('backend')}}/css/fullcalendar.min.css">
    <!-- Animate CSS -->
    <link rel="stylesheet" href="{{asset('backend')}}/css/animate.min.css">
    <!-- Select 2 CSS -->
    <link rel="stylesheet" href="{{asset('backend')}}/css/select2.min.css">
    <!-- Date Picker CSS -->
    <link rel="stylesheet" href="{{asset('backend')}}/css/datepicker.min.css">
    {{-- Toster --}}
    <link rel="stylesheet" href="{{asset('backend')}}/css/toastr.min.css" />
    <!-- Data Table CSS -->
    <link rel="stylesheet" href="{{asset('backend')}}/css/jquery.dataTables.min.css">
    <!-- Jquery UI CSS -->
    <link rel="stylesheet" href="{{asset('backend')}}/css/jquery-ui.css">
    <!-- Custom CSS -->
    <link rel="stylesheet" href="{{asset('backend')}}/style.css">
    @stack('css')

    <!-- Modernize js -->
    <script src="{{asset('backend')}}/js/modernizr-3.6.0.min.js"></script>
</head>

<body>
    <!-- Preloader Start Here -->
    <div id="preloader"></div>
    <!-- Preloader End Here -->
    <div id="wrapper" class="wrapper bg-ash">
       <!-- Header Menu Area Start Here -->


        @include('backend.header_menu')


        <!-- Header Menu Area End Here -->
        <!-- Page Area Start Here -->
        <div class="dashboard-page-one">
            <!-- Sidebar Area Start Here -->
                
                @include('backend.sidebar')

            <!-- Sidebar Area End Here -->
            <div class="dashboard-content-one">
                <!-- main content area start here  -->

                    @yield('content')
                
                <!-- main content area End here  -->
                <!-- Footer Area Start Here -->


                @include('backend.footer')



                <!-- Footer Area End Here -->
            </div>
        </div>
        <!-- Page Area End Here -->
    </div>
    <!-- jquery-->
    <script src="{{asset('backend')}}/js/jquery-3.3.1.min.js"></script>
    <!-- jquery UI-->
    <script src="{{asset('backend')}}/js/jquery-ui.min.js"></script>
    <!-- Plugins js -->
    <script src="{{asset('backend')}}/js/plugins.js"></script>
    <!-- Popper js -->
    <script src="{{asset('backend')}}/js/popper.min.js"></script>
    <!-- Bootstrap js -->
    <script src="{{asset('backend')}}/js/bootstrap.min.js"></script>
    <!-- Counterup Js -->
    <script src="{{asset('backend')}}/js/jquery.counterup.min.js"></script>
    <!-- Moment Js -->
    <script src="{{asset('backend')}}/js/moment.min.js"></script>
    <!-- Waypoints Js -->
    <script src="{{asset('backend')}}/js/jquery.waypoints.min.js"></script>
    <!-- Scroll Up Js -->
    <script src="{{asset('backend')}}/js/jquery.scrollUp.min.js"></script>
    <!-- Full Calender Js -->
    <script src="{{asset('backend')}}/js/fullcalendar.min.js"></script>
    <!-- Chart Js -->
    <script src="{{asset('backend')}}/js/Chart.min.js"></script>
    <!-- Select 2 Js -->
    <script src="{{asset('backend')}}/js/select2.min.js"></script>
    <!-- Date Picker Js -->
    <script src="{{asset('backend')}}/js/datepicker.min.js"></script>
    {{-- Toster --}}
    <script src="{{asset('backend')}}/js/toastr.min.js" ></script>
    <!-- Data Table Js -->
    <script src="{{asset('backend')}}/js/jquery.dataTables.min.js"></script>
    {{-- Handlebars js --}}
    <script type="text/JavaScript" src="{{asset('backend')}}/js/handlebars.min.js" ></script>
    <!-- Custom Js -->
    <script src="{{asset('backend')}}/js/main.js"></script>
    
    @yield('script')

    
    {{-- Code for delete button --}}
    <script type="text/javascript">
        function check_delete(){
            var chk = confirm("Are you sure DELETE this???");
            if (chk) {
            return true;
            } else {
            return false;
            }
        }
    </script>

    {{-- code for Toster --}}
    <script type="text/javascript">
        @if (Session::has('success')) 
        toastr.success("{{ Session::get('success')}}");
        @elseif(Session::has('error'))
        toastr.error("{{ Session::get('error')}}");
        @elseif(Session::has('info'))
        toastr.info("{{ Session::get('info')}}");
        @elseif(Session::has('warning'))
            toastr.warning("{{ Session::get('warning')}}");
        @endif
    </script>
</body>
</html>