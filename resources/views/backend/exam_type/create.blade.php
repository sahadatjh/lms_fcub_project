@extends('backend.master')
@section('content')
<!-- Breadcubs Area Start Here -->
<div class="breadcrumbs-area">
    <ul>
        <li>
            <a href="{{route('dashboard')}}">{{__('Home')}}</a>
        </li>
        <li>{{__('Add New Exam Type')}}</li>
    </ul>
</div>
<!-- Breadcubs Area End Here -->
<!-- Exam Type Area Start Here -->
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="heading-layout1">
                    <div class="item-title">
                        <h3>{{__('Add New Exam Type')}}</h3>
                    </div>
                    <div class="pull-right">
                        <a href="{{route('exam.type.index')}}"><button class="btn-fill-lg font-normal text-light gradient-orange-peel">{{__('All')}}</button></a>
                    </div>
                </div>
                <hr><hr>
                <form method="POST" action="{{ route('exam.type.store') }}" class="new-added-form">
                    @csrf
                    @if ($errors->any())
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    @endif
                    <div class="row">
                        <div class="col-lg-5 col-12 form-group">
                            <label for="exam_type_name">{{__('Exam Type *')}}</label>
                            <input id="exam_type_name" type="text" name="exam_type_name" class="form-control @error('exam_type_name') is-invalid @enderror"  value="{{ old('exam_type_name') }}" placeholder="Ex. Mid Term"  autocomplete="exam_type_name" required >
                        </div>
                        <div class=" col-lg-5 col-12 form-group">
                            <label for="activation_status">{{__('Activation Status *')}}</label>
                            <select name="activation_status" id="activation_status" class="select2  @error('activation_status') is-invalid @enderror" required>
                                <option value="">{{__('Please Select *')}}</option>
                                <option value="1" selected>{{__('Active')}}</option>
                                <option value="0">{{__('Inactive')}}</option>
                            </select>
                        </div>
                        <div class="col-2 form-group mg-t-15">
                            <button type="submit" class="btn-fill-lg bg-blue-dark btn-hover-yellow mg-t-22">{{__('Save')}}</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Exam Type Area End Here -->
@endsection