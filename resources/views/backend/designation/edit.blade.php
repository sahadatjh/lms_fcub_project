@extends('backend.master')
@section('content')
<!-- Breadcubs Area Start Here -->
<div class="breadcrumbs-area">
    <ul>
        <li>
            <a href="{{route('dashboard')}}">{{__('Home')}}</a>
        </li>
        <li>{{__('Edit Designation')}}</li>
    </ul>
</div>
<!-- Breadcubs Area End Here -->
<!-- Designation Edit Area Start Here -->
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="heading-layout1">
                    <div class="item-title">
                        <h3>{{__('Edit Designation Information')}}</h3>
                    </div>
                    <div class="pull-right">
                        <a href="{{route('designation.index')}}"><button class="btn-fill-lg font-normal text-light gradient-orange-peel">{{__('All Designation')}}</button></a>
                    </div>
                </div>
                <hr><hr>
                <form method="POST" action="{{ route('designation.update',$designation->id) }}" class="new-added-form">
                    @csrf
                    @if ($errors->any())
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    @endif
                    <div class="row">
                        <div class="col-lg-4 col-12 form-group">
                            <label for="title">{{__('designation title *')}}</label>
                            <input id="title" type="text" name="title" class="form-control @error('title') is-invalid @enderror"  value="{{ $designation->title }}" >
                        </div>
                        <div class=" col-lg-4 col-12 form-group">
                            <label for="activation_status">{{__('Activation Status *')}}</label>
                            <select name="activation_status" id="activation_status" class="select2  @error('activation_status') is-invalid @enderror">
                                <option value="">{{__('Please Select *')}}</option>
                                <option value="1" {{(1==$designation->activation_status)?'selected':''}}>{{__('Active')}}</option>
                                <option value="0" {{(0==$designation->activation_status)?'selected':''}}>{{__('Inactive')}}</option>
                            </select>
                        </div>
                        <div class="col-2 form-group mg-t-20">
                            <button type="submit" class="btn-fill-lg bg-blue-dark btn-hover-yellow mg-t-17">{{__('Update')}}</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Designation Edit Area End Here -->
@endsection