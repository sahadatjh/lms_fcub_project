@extends('backend.master')
@section('content')
<!-- Breadcubs Area Start Here -->
<div class="breadcrumbs-area">
    <ul>
        <li>
            <a href="{{route('dashboard')}}">{{__('Home')}}</a>
        </li>
        <li>{{__('All Fee Amount By Category')}}</li>
    </ul>
</div>
<!-- Breadcubs Area End Here -->
<!-- Fee Amount by category Area Start Here -->
<div class="card height-auto">
    <div class="card-body">
        <div class="heading-layout1">
            <div class="item-title">
                <h3>{{__('All Fee Amount By Category')}}</h3>
            </div>
            <div class="pull-right">
                <a href="{{route('fee.amount.create')}}"><button class="btn-fill-lg font-normal text-light gradient-orange-peel">{{__('Add New')}}</button></a>
            </div>
        </div>
        <hr><hr>
        <div class="table-responsive">
            <table class="table display data-table text-nowrap">
                <thead>
                    <tr>
                        <th>{{__('Sl No')}}</th>
                        <th>{{__('Fee Category')}}</th>
                        <th>{{__('Created By')}}</th>
                        <th>{{__('Action')}}</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($fee_amount as $key => $item)
                    <tr>
                        <td>{{$key+1}}</td>
                        <td>{{$item->feeCategory->category_name}}</td>
                        <td>{{$item->createdUser->username}}</td>
                        <td>
                            <a href="#" class="btn btn-info" title="show"><i class="fa fa-eye"></i></a>
                            <a href="{{route('fee.amount.edit',1)}}" class="btn btn-info" title="Edit"><i class="fas fa-edit"></i></a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<!-- Fee Amount by category Area End Here -->
@endsection