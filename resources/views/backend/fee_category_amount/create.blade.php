@extends('backend.master')
@section('content')
<!-- Breadcubs Area Start Here -->
<div class="breadcrumbs-area">
    <ul>
        <li>
            <a href="{{route('dashboard')}}">{{__('Home')}}</a>
        </li>
        <li>{{__('Add New Category')}}</li>
    </ul>
</div>
<!-- Breadcubs Area End Here -->
<!--Add Fee Amount Area Start Here -->
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="heading-layout1">
                    <div class="item-title">
                        <h3>{{__('Add New Fee Amount By Catecory')}}</h3>
                    </div>
                    <div class="pull-right">
                        <a href="{{route('fee.amount.index')}}"><button class="btn-fill-lg font-normal text-light gradient-orange-peel">{{__('All Amount')}}</button></a>
                    </div>
                </div>
                <hr><hr>
                @if ($errors->any())
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span>
                    </button>
                </div>
                @endif
                <div class="row new-added-form">
                    <div class=" col-lg-4 col-12 form-group ">
                        <label for="fee_category_id">{{__('Fee Category *')}}</label>
                        <select name="fee_category_id[]" id="fee_category_id" class="select2  @error('fee_category_id') is-invalid @enderror" required>
                            <option value="">{{__('Please Select *')}}</option>
                            @foreach ($categories as $item)
                                <option value="{{$item->id}}">{{$item->category_name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class=" col-lg-4 col-12 form-group ">
                        <label for="fee_amount">{{__('Fee Amount')}}</label>
                        <input type="number"  name="fee_amount[]" class="form-control" placeholder="Ex. 500">
                    </div>
                    <div class="col-2 form-group mg-t-8">
                        <button class="btn-fill-lg bg-blue-dark btn-hover-yellow btnAdd mg-t-30 " id="btnAdd">Add</button>
                    </div>
                </div>
                <div class="col-12 form-group mg-t-8">
                    <button type="submit" class="btn-fill-lg btn-gradient-yellow btn-hover-bluedark" id="btnStore">Submit</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Add Fee Amount Area End Here -->
@endsection


{{-- Script Section --}}
@section('script')
    <script>
        // var html=$("#hi").html();
        var html = '<div class=" col-lg-4 col-12 form-group "><label for="fee_amount">{{__("Fee Amount")}}</label><input type="number"  name="fee_amount[]" class="form-control" placeholder="Ex. 500"></div>';
        $('#btnAdd').click(function(){
        $('.new-added-form').append(html);
    });
    </script>
@endsection
