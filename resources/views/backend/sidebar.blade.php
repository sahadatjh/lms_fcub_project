@php
    $prefix=Request::route()->getPrefix();
    $route=Route::current()->getName();
@endphp
<div class="sidebar-main sidebar-menu-one sidebar-expand-md sidebar-color">
    <div class="mobile-sidebar-header d-md-none">
        <div class="header-logo">
            <a href="index.html"><img src="{{asset('public/backend')}}/img/logo1.png" alt="logo"></a>
        </div>
    </div>
    <div class="sidebar-menu-content">
        <ul class="nav nav-sidebar-menu sidebar-toggle-view">
            <li class="nav-item sidebar-nav-item">
                <a href="#" class="nav-link"><i class="flaticon-dashboard"></i><span>{{__('Dashboard')}}</span></a>
                <ul class="nav sub-group-menu {{($route=='dashboard')?'sub-group-active':''}} ">
                    <li class="nav-item">
                        <a href="{{route('dashboard')}}" class="nav-link {{($route=='dashboard')?'menu-active':''}}"><i class="fas fa-angle-right"></i>{{__('Admin')}}</a>
                    </li>
                    <li class="nav-item">
                        <a href="#" class="nav-link"><i class="fas fa-angle-right"></i>Students</a>
                    </li>
                    <li class="nav-item">
                        <a href="#" class="nav-link"><i class="fas fa-angle-right"></i>Parents</a>
                    </li>
                    <li class="nav-item">
                        <a href="#" class="nav-link"><i class="fas fa-angle-right"></i>Teachers</a>
                    </li>
                </ul>
            </li>
            <li class="nav-item sidebar-nav-item">
                <a href="#" class="nav-link"><i class="flaticon-settings"></i><span>{{__('Initial Settings')}}</span></a>
                <ul class="nav sub-group-menu {{($prefix=='/settings')?'sub-group-active':''}}">
                    <li class="nav-item">
                        <a href="{{route('cariculam.index')}}" class="nav-link {{($route=='cariculam.index')?'menu-active':''}}"><i class="fas fa-angle-right"></i>{{ __('All Cariculams')}}</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('department.index')}}" class="nav-link {{($route=='department.index')?'menu-active':''}}"><i class="fas fa-angle-right"></i>{{ __('All Departments')}}</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('semester.index')}}" class="nav-link {{($route=='semester.index')?'menu-active':''}}"><i class="fas fa-angle-right"></i>{{ __('All Semesters')}}</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('session.index')}}" class="nav-link {{($route=='session.index')?'menu-active':''}}"><i class="fas fa-angle-right"></i>{{ __('All Sessions')}}</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('designation.index')}}" class="nav-link {{($route=='designation.index')?'menu-active':''}}"><i class="fas fa-angle-right"></i>{{ __('All Designations')}}</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('author.index')}}" class="nav-link {{($route=='author.index')?'menu-active':''}}"><i class="fas fa-angle-right"></i>{{__('All Authors')}}</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('publication.index')}}" class="nav-link {{($route=='publication.index')?'menu-active':''}}"><i class="fas fa-angle-right"></i>{{ __('All Publications')}}</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('fee.category.index')}}" class="nav-link {{($route=='fee.category.index')?'menu-active':''}}"><i class="fas fa-angle-right"></i>{{ __('Fee Categories')}}</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('fee.amount.index')}}" class="nav-link {{($route=='fee.amount.index')?'menu-active':''}}"><i class="fas fa-angle-right"></i>{{ __('Fee Amount')}}</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('exam.type.index')}}" class="nav-link {{($route=='exam.type.index')?'menu-active':''}}"><i class="fas fa-angle-right"></i>{{ __('Exam Types')}}</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('book.index')}}" class="nav-link {{($route=='book.index')?'menu-active':''}}"><i class="fas fa-angle-right"></i>{{ __('All Books')}}</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('assign.subject.index')}}" class="nav-link {{($route=='assign.subject.index')?'menu-active':''}}"><i class="fas fa-angle-right"></i>{{ __('Assign Subject')}}</a>
                    </li>
                </ul>
            </li>
            <li class="nav-item sidebar-nav-item">
                <a href="#" class="nav-link"><i class="flaticon-mortarboard"></i><span>Students</span></a>
                <ul class="nav sub-group-menu {{($prefix=='/student')?'sub-group-active':''}}">
                    <li class="nav-item">
                        <a href="{{route('student.index')}}" class="nav-link {{($route=='student.index')?'menu-active':''}}"><i class="fas fa-angle-right"></i>All Students</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('student.create')}}" class="nav-link {{($route=='student.create')?'menu-active':''}}"><i class="fas fa-angle-right"></i>Admission Form</a>
                    </li>
                    <li class="nav-item">
                        <a href="#" class="nav-link"><i class="fas fa-angle-right {{($route=='student.index')?'menu-active':''}}"></i>Student Promotion</a>
                    </li>
                </ul>
            </li>
            <li class="nav-item sidebar-nav-item">
                <a href="#" class="nav-link"><i class="flaticon-books"></i><span>Library</span></a>
                <ul class="nav sub-group-menu {{($prefix=='/book')?'sub-group-active':''}}">
                    <li class="nav-item">
                        <a href="{{route('book.index')}}" class="nav-link {{($route=='book.index')?'menu-active':''}}"><i class="fas fa-angle-right"></i>All Subject</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('book.create')}}" class="nav-link {{($route=='book.create')?'menu-active':''}}"><i class="fas fa-angle-right"></i>Add New Subject</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('purchase.create')}}" class="nav-link {{($route=='purchase.create')?'menu-active':''}}"><i class="fas fa-angle-right"></i>Purchase New Book</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('purchase.index')}}" class="nav-link {{($route=='purchase.index')?'menu-active':''}}"><i class="fas fa-angle-right"></i>All Purchase List</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('distribution.create')}}" class="nav-link {{($route=='distribution.create')?'menu-active':''}}"><i class="fas fa-angle-right"></i>Book Issue</a>
                        <a href="{{route('distribution.index')}}" class="nav-link {{($route=='distribution.index')?'menu-active':''}}"><i class="fas fa-angle-right"></i>View & Return</a>
                    </li>
                </ul>
            </li>
            @if(Auth::user()->role=="Super Admin")
            <li class="nav-item sidebar-nav-item">
                <a href="#" class="nav-link"><i class="flaticon-multiple-users-silhouette"></i><span>Users</span></a>
                <ul class="nav sub-group-menu {{($prefix=='/user')?'sub-group-active':''}}">
                    <li class="nav-item">
                        <a href="{{route('user.index')}}" class="nav-link {{($route=='user.index')?'menu-active':''}}"><i class="fas fa-angle-right"></i>All User</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('user.create')}}" class="nav-link {{($route=='user.create')?'menu-active':''}}"><i class="fas fa-angle-right"></i>Add User</a>
                    </li>
                    <li class="nav-item">
                        <a href="{{route('user.password')}}" class="nav-link {{($route=='user.password')?'menu-active':''}}"><i class="fas fa-angle-right"></i>Change Password</a>
                    </li>
                </ul>
            </li>
            @endif
            
        </ul>
    </div>
</div>
