@extends('backend.master')
@section('content')
<!-- Breadcubs Area Start Here -->
<div class="breadcrumbs-area">
    <ul>
        <li>
            <a href="{{route('dashboard')}}">{{__('Home')}}</a>
        </li>
        <li>{{__('Add New Author')}}</li>
    </ul>
</div>
<!-- Breadcubs Area End Here -->
<!-- Add Author Area Start Here -->
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="heading-layout1">
                    <div class="item-title">
                        <h3>{{__('Add New Author')}}</h3>
                    </div>
                    <div class="pull-right">
                        <a href="{{route('author.index')}}"><button class="btn-fill-lg font-normal text-light gradient-orange-peel">{{__('All Author')}}</button></a>
                    </div>
                </div>
                <hr><hr>
                <form method="POST" action="{{ route('author.store') }}" class="new-added-form">
                    @csrf
                    @if ($errors->any())
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    @endif
                    <div class="row">
                        <div class="col-xl-4 col-lg-4 col-12 form-group">
                            <label for="name">{{__('Author Name *')}}</label>
                            <input id="name" type="text" name="name" class="form-control @error('name') is-invalid @enderror"  value="{{ old('name') }}"placeholder="Ex. Dr. Md. Sahadat Hossain" autofocus  autocomplete="name" >
                        </div>
                        <div class="col-xl-4  col-lg-4 col-12 form-group">
                            <label for="activation_status">{{__('Activation Status *')}}</label>
                            <select name="activation_status" id="activation_status" class="select2  @error('activation_status') is-invalid @enderror">
                                <option value="">{{__('Please Select *')}}</option>
                                <option value="1" selected>{{__('Active')}}</option>
                                <option value="0">{{__('Inactive')}}</option>
                            </select>
                        </div>
                        <div class="col-xl-4 col-lg-4 col-12 form-group">
                            <label for="coments">{{__('Coments/Title *')}}</label>
                            <textarea id="coments" name="coments" class="textarea form-control @error('coments') is-invalid @enderror" cols="10" rows="4"></textarea>
                        </div>
                        <div class="col-12 form-group mg-t-8">
                            <button type="submit" class="btn-fill-lg btn-gradient-yellow btn-hover-bluedark">{{__('Save')}}</button>
                            <button type="reset" class="btn-fill-lg bg-blue-dark btn-hover-yellow">{{__('Reset')}}</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Add Author Area Start Here -->
@endsection