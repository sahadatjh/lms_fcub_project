@extends('backend.master')
@section('content')
<!-- Breadcubs Area Start Here -->
<div class="breadcrumbs-area">
    {{-- <h3>Semesters</h3> --}}
    <ul>
        <li>
            <a href="{{route('dashboard')}}">{{ __('Home')}}</a>
        </li>
        <li>{{ __('All Semester')}}</li>
    </ul>
</div>
<!-- Breadcubs Area End Here -->
<!-- Semester Table Area Start Here -->
<div class="card height-auto">
    <div class="card-body">
        <div class="heading-layout1">
            <div class="item-title">
                <h3>{{ __('All Semesters')}}</h3>
            </div>
            <div class="pull-right">
                <a href="{{route('semester.create')}}"><button class="btn-fill-lg font-normal text-light gradient-orange-peel">{{ __(' Add New')}}</button></a>
            </div>
        </div>
        <hr><hr>
        <div class="table-responsive">
            <table class="table display data-table text-nowrap">
                <thead>
                    <tr>
                        <th>
                            <div class="form-check">
                                <input type="checkbox" class="form-check-input checkAll">
                                <label class="form-check-label">{{ __('Sl No')}}</label>
                            </div>
                        </th>
                        <th>{{ __('Semester')}}</th>
                        <th>{{ __('Code')}}</th>
                        <th>{{ __('Created By')}}</th>
                        <th>{{ __('Status')}}</th>
                        <th>{{ __('Action')}}</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($semesters as $key => $item)
                    <tr>
                        <td>
                            <div class="form-check">
                                <input type="checkbox" class="form-check-input">
                                <label class="form-check-label">{{$key+1}}</label>
                            </div>
                        </td>
                        <td>{{$item->name}}</td>
                        <td>{{$item->semester_code}}</td>
                        <td>{{$item->createdUser->username}}</td>
                        <td>
                            @if (1==$item->activation_status)
                                <span class="badge badge-pill badge-success">{{ __('Active')}}</span>
                            @elseif(0==$item->activation_status)
                                <span class="badge badge-pill badge-warning">{{ __('Inactive')}}</span>
                            @endif
                        </td>
                        <td>
                            <a href="{{route('semester.edit',$item->id)}}" class="btn btn-info" title="Edit"><i class="fas fa-edit"></i></a>
                            <a href="{{route('semester.destroy',$item->id)}}" id="delete"class="btn btn-danger" onclick="return check_delete();"title="Delete"><i class="fa fa-trash" aria-hidden="true"></i></a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<!-- Semester Table Area End Here -->
@endsection