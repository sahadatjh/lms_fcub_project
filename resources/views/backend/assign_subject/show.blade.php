@extends('backend.master')
@section('content')
<!-- Breadcubs Area Start Here -->
<div class="breadcrumbs-area">
    <ul>
        <li>
            <a href="{{route('dashboard')}}">{{__('Home')}}</a>
        </li>
        <li>{{__('All Assigned Subject')}}</li>
    </ul>
</div>
<!-- Breadcubs Area End Here -->
<!-- Assigned Subject Area Start Here -->
<div class="card height-auto">
    <div class="card-body">
        <div class="heading-layout1">
            <div class="item-title">
                <h3>{{$department->name}} ==>{{$semester->name}}</h3>
            </div>
            <div class="pull-right">
                <a href="{{route('assign.subject.index')}}"><button class="btn-fill-lg font-normal text-light gradient-orange-peel">{{__('All Assigned')}}</button></a>
            </div>
        </div>
        <hr>
        <div class="table-responsive">
            <table class="table display text-nowrap">
                <thead>
                    <tr>
                        <th>{{__('Sl No')}}</th>
                        <th>{{__('Subject')}}</th>
                        <th>{{__('TC')}}</th>
                        <th>{{__('TF')}}</th>
                        <th>{{__('PC')}}</th>
                        <th>{{__('PF')}}</th>
                        <th>{{__('Total')}}</th>
                        <th>{{__('Pass Mark')}}</th>
                        <th>{{__('Action')}}</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($assign_subject_by_semester as $key => $item)
                    <tr>
                        <td>{{$key+1}}</td>
                        <td>{{$item->subject->name}}</td>
                        <td>{{$item->tc_mark}}</td>
                        <td>{{$item->tf_mark}}</td>
                        <td>{{$item->pc_mark}}</td>
                        <td>{{$item->pf_mark}}</td>
                        <td>{{$item->total_mark}}</td>
                        <td>{{$item->total_pass_mark}}</td>
                        <td>
                            <a href="{{route('assign.subject.edit',$item->id)}}" class="btn btn-info" title="Edit"><i class="fas fa-edit"></i></a>
                            <a href="{{route('assign.subject.destroy',$item->id)}}" id="delete"class="btn btn-danger" onclick="return check_delete();"title="Delete"><i class="fa fa-trash" aria-hidden="true"></i></a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<!-- Assigned Subject Area End Here -->
@endsection