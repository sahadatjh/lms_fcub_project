@extends('backend.master')
@section('content')
<!-- Breadcubs Area Start Here -->
<div class="breadcrumbs-area">
    <ul>
        <li>
            <a href="{{route('dashboard')}}">{{__('Home')}}</a>
        </li>
        <li>{{__('All Assigned Subject')}}</li>
    </ul>
</div>
<!-- Breadcubs Area End Here -->
<!-- Assigned Subject Area Start Here -->
<div class="card height-auto">
    <div class="card-body">
        <div class="heading-layout1">
            <div class="item-title">
                <h3>{{__('All Assigned Subject')}}</h3>
            </div>
            <div class="pull-right">
                <a href="{{route('assign.subject.create')}}"><button class="btn-fill-lg font-normal text-light gradient-orange-peel">{{__('New Assign')}}</button></a>
            </div>
        </div>
        <hr><hr>
        <div class="table-responsive">
            <table class="table display data-table text-nowrap">
                <thead>
                    <tr>
                        <th>{{__('Sl No')}}</th>
                        <th>{{__('Department')}}</th>
                        <th>{{__('Semester')}}</th>
                        <th>{{__('Action')}}</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($assign_subjects as $key => $item)
                    <tr>
                        <td>{{$key+1}}</td>
                        <td>{{$item->department->name}}</td>
                        <td>{{$item->semester->name}}</td>
                        <td>
                            <a href="{{route('assign.subject.show',['department_id'=>$item->department_id,'semester_id'=>$item->semester_id])}}" class="btn btn-info" title="show"><i class="fa fa-eye"></i></a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<!-- Assigned Subject Area End Here -->
@endsection