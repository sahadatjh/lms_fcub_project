@extends('backend.master')
@section('content')
<!-- Breadcubs Area Start Here -->
<div class="breadcrumbs-area">
    <ul>
        <li>
            <a href="{{route('dashboard')}}">{{__('Home')}}</a>
        </li>
        <li>{{__('Assign Subject Information Update')}}</li>
    </ul>
</div>
    <!-- Assign Subject Information Update Area Start Here -->
    <div class="row">
        <div class="col-4-xxxl col-12">
            <div class="card height-auto">
                <div class="card-body">
                    <div class="heading-layout1">
                        <div class="item-title">
                            <h3>{{__('Assign Subject Information Update')}}</h3>
                        </div>
                        <div class="pull-right">
                            <a href="{{route('assign.subject.index')}}"><button class="btn-fill-lg font-normal text-light gradient-orange-peel">{{__('All Assigned')}}</button></a>
                        </div>
                    </div>
                    <hr><hr>
                    <form method="POST" action="{{ route('assign.subject.update',$assign_subject->id) }}" class="new-added-form">
                        @csrf
                        @if ($errors->any())
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <ul>
                                @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        @endif
                        <div class="row">
                            <div class=" col-lg-6 col-12 form-group">
                                <label for="department_id">{{__('Select Department *')}}</label>
                                <select name="department_id" id="department_id" class="select2  @error('department_id') is-invalid @enderror" required>
                                    <option value="">{{__('Please Select *')}}</option>
                                    @foreach ($departments as $item)
                                        <option value="{{$item->id}}" {{($item->id==$assign_subject->department_id)?'selected':''}}>{{$item->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class=" col-lg-6 col-12 form-group">
                                <label for="semester_id">{{__('Select Semester *')}}</label>
                                <select name="semester_id" id="semester_id" class="select2  @error('semester_id') is-invalid @enderror" required>
                                    <option value="">{{__('Please Select *')}}</option>
                                    @foreach ($semesters as $item)
                                        <option value="{{$item->id}}" {{ $assign_subject->semester_id == $item->id ?'selected':'' }}>{{$item->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class=" col-lg-4 col-12 form-group">
                                <label for="subject_id">{{__('Select Subject *')}}</label>
                                <select name="subject_id" id="subject_id" class="select2  @error('subject_id') is-invalid @enderror" required>
                                    <option value="">{{__('Please Select *')}}</option>
                                    @foreach ($subjects as $item)
                                        <option value="{{$item->id}}" {{($item->id==$assign_subject->subject_id)?'selected':''}}>{{$item->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class=" col-lg-2 col-12 form-group">
                                <label for="tc_mark">{{__('TC (Mark) *')}}</label>
                                <input id="tc_mark" type="number" name="tc_mark" class="form-control @error('tc_mark') is-invalid @enderror"  value="{{ $assign_subject->tc_mark }}">
                            </div>
                            <div class=" col-lg-2 col-12 form-group">
                                <label for="tc_pass_mark">{{__('Pass (Mark) *')}}</label>
                                <input id="tc_pass_mark" type="number" name="tc_pass_mark" class="form-control @error('tc_pass_mark') is-invalid @enderror"  value="{{$assign_subject->tc_pass_mark}}">
                            </div>
                            <div class=" col-lg-2 col-12 form-group">
                                <label for="tf_mark">{{__('TF (Mark) *')}}</label>
                                <input id="tf_mark" type="number" name="tf_mark" class="form-control @error('tf_mark') is-invalid @enderror"  value="{{$assign_subject->tf_mark }}">
                            </div>
                            <div class=" col-lg-2 col-12 form-group">
                                <label for="tf_pass_mark">{{__('Pass (Mark) *')}}</label>
                                <input id="tf_pass_mark" type="number" name="tf_pass_mark" class="form-control @error('tf_pass_mark') is-invalid @enderror"  value="{{$assign_subject->tf_pass_mark}}">
                            </div>
                            <div class=" col-lg-2 col-12 form-group">
                                <label for="pc_mark">{{__('PC (Mark) *')}}</label>
                                <input id="pc_mark" type="number" name="pc_mark" class="form-control @error('pc_mark') is-invalid @enderror"  value="{{$assign_subject->pc_mark }}">
                            </div>
                            <div class=" col-lg-2 col-12 form-group">
                                <label for="pc_pass_mark">{{__('Pass (Mark) *')}}</label>
                                <input id="pc_pass_mark" type="number" name="pc_pass_mark" class="form-control @error('pc_pass_mark') is-invalid @enderror"  value="{{$assign_subject->pc_pass_mark}}">
                            </div>
                            <div class=" col-lg-2 col-12 form-group">
                                <label for="pf_mark">{{__('PF (Mark) *')}}</label>
                                <input id="pf_mark" type="number" name="pf_mark" class="form-control @error('pf_mark') is-invalid @enderror"  value="{{$assign_subject->pf_mark}}">
                            </div>
                            <div class=" col-lg-2 col-12 form-group">
                                <label for="pf_pass_mark">{{__('Pass (Mark) *')}}</label>
                                <input id="pf_pass_mark" type="number" name="pf_pass_mark" class="form-control @error('pf_pass_mark') is-invalid @enderror"  value="{{$assign_subject->pf_pass_mark }}">
                            </div>
                            <div class=" col-lg-2 col-12 form-group">
                                <label for="total_mark">{{__('Total *')}}</label>
                                <input id="total_mark" type="number" name="total_mark" class="form-control @error('total_mark') is-invalid @enderror"  value="{{$assign_subject->total_mark }}">
                            </div>
                            <div class=" col-lg-2 col-12 form-group">
                                <label for="total_pass_mark">{{__('Pass (Total) *')}}</label>
                                <input id="total_pass_mark" type="number" name="total_pass_mark" class="form-control @error('total_pass_mark') is-invalid @enderror"  value="{{$assign_subject->total_pass_mark}}">
                            </div>
                            <div class="col-12 form-group mg-t-8">
                                <button type="submit" class="btn-fill-lg btn-gradient-yellow btn-hover-bluedark">Update</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Assign Subject Information Update Area End Here -->
@endsection

