@extends('backend.master')
@section('content')
<!-- Breadcubs Area Start Here -->
<div class="breadcrumbs-area">
    <ul>
        <li>
            <a href="{{route('dashboard')}}">{{__('Home')}}</a>
        </li>
        <li>{{__('Edit Fee amount')}}</li>
    </ul>
</div>
<!-- Breadcubs Area End Here -->
<!-- Fee Amount Edit Area Start Here -->
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="heading-layout1">
                    <div class="item-title">
                        <h3>{{__('Edit Fee Amount Information')}}</h3>
                    </div>
                    <div class="pull-right">
                        <a href="{{route('fee.amount.index')}}"><button class="btn-fill-lg font-normal text-light gradient-orange-peel">{{__('All Amount')}}</button></a>
                    </div>
                </div>
                <hr><hr>
                <div class="table-responsive">
                    <h2 class="text-center">{{$department->name}}</h2>
                    <table class="table display text-nowrap">
                        <thead>
                            <tr>
                                <th>{{__('SL No')}}</th>
                                <th>{{__('Fee Category')}}</th>
                                <th>{{__('Amount')}}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($fee_amount as $key => $item)
                            <tr>
                                <td>{{$key+1}}</td>
                                <td>{{$item->feeCategory->category_name}}</td>
                                <td>{{ $item->amount }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table><hr>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Fee amount Edit Area End Here -->
@endsection