@extends('backend.master')
@section('content')
<!-- Breadcubs Area Start Here -->
<div class="breadcrumbs-area">
    <ul>
        <li>
            <a href="{{route('dashboard')}}">{{__('Home')}}</a>
        </li>
        <li>{{__('Add New Category')}}</li>
    </ul>
</div>
<!-- Breadcubs Area End Here -->
<!--Add Fee Amount Area Start Here -->
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="heading-layout1">
                    <div class="item-title">
                        <h3>{{__('Add New Fee Amount By Catecory')}}</h3>
                    </div>
                    <div class="pull-right">
                        <a href="{{route('fee.amount.index')}}"><button class="btn-fill-lg font-normal text-light gradient-orange-peel">{{__('All Amount')}}</button></a>
                    </div>
                </div>
                <hr><hr>
                <form method="POST" action="{{ route('fee.amount.store') }}" class="new-added-form">
                    @csrf
                    @if ($errors->any())
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    @endif
                    <div class="row">
                        <div class=" col-lg-6 col-12 form-group">
                            <label for="cariculam_id">{{__('Cariculam *')}}</label>
                            <select name="cariculam_id" id="cariculam_id" class="select2  @error('cariculam_id') is-invalid @enderror" required>
                                <option value="">{{__('Please Select *')}}</option>
                                @foreach ($cariculams as $item)
                                    <option value="{{$item->id}}">{{$item->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class=" col-lg-6 col-12 form-group">
                            <label for="department_id">{{__('Department *')}}</label>
                            <select name="department_id" id="department_id" class="select2  @error('department_id') is-invalid @enderror" required>
                                <option value="">{{__('Please Select *')}}</option>
                                @foreach ($departments as $item)
                                    <option value="{{$item->id}}">{{$item->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div><hr>
                    <div class="table-responsive">
                        <table class="table display text-nowrap">
                            <thead>
                                <tr>
                                    <th>{{__('Sl No')}}</th>
                                    <th>{{__('Category')}}</th>
                                    <th>{{__('Amount')}}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($categories as $key => $item)
                                <tr>
                                    <td>{{$key+1}}</td>
                                    <td>{{$item->category_name}}</td>
                                    <td>
                                        <div class="form-group">
                                            <input type="hidden" name="fee_category_id[]" value="{{$item->id}}">
                                            <input id="amount" type="number" name="amount[]" class="form-control" placeholder="Ex. 500" required >
                                        </div>
                                    </td>
                                @endforeach
                            </tbody>
                        </table><hr>
                        <div class="col-12 form-group mg-t-8">
                            <button type="submit" class="btn-fill-lg btn-gradient-yellow btn-hover-bluedark">Save</button>
                            <button type="reset" class="btn-fill-lg bg-blue-dark btn-hover-yellow">Reset</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Add Fee Amount Area End Here -->
@endsection