@extends('backend.master')
@section('content')
<!-- Breadcubs Area Start Here -->
<div class="breadcrumbs-area">
    <ul>
        <li>
            <a href="{{route('dashboard')}}">{{__('Home')}}</a>
        </li>
        <li>{{__('Edit Fee amount')}}</li>
    </ul>
</div>
<!-- Breadcubs Area End Here -->
<!-- Fee Amount Edit Area Start Here -->
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="heading-layout1">
                    <div class="item-title">
                        <h3>{{__('Edit Fee Amount Information')}}</h3>
                    </div>
                    <div class="pull-right">
                        <a href="{{route('fee.amount.index')}}"><button class="btn-fill-lg font-normal text-light gradient-orange-peel">{{__('All Amount')}}</button></a>
                    </div>
                </div>
                <hr><hr>
                <form method="POST" action="{{ route('fee.amount.update',$department->id) }}" class="new-added-form">
                    @csrf
                    @if ($errors->any())
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    @endif
                    <div class="table-responsive">
                    <h2 class="text-center">{{$department->name}} <input type="hidden" name="department_id" value="{{$department->id}}"></h2>
                        <table class="table display text-nowrap">
                            <thead>
                                <tr>
                                    <th>{{__('SL No')}}</th>
                                    <th>{{__('Fee Category')}}</th>
                                    <th>{{__('Amount')}}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($fee_amount as $key => $item)
                                <tr>
                                    <td>{{$key+1}}</td>
                                    <td>
                                        {{$item->feeCategory->category_name}}
                                        <input type="hidden" name="fee_category_id[]" value="{{$item->fee_category_id}}">
                                    </td>
                                    <td>
                                        <div class="form-group">
                                            <input id="amount" type="number" name="amount[]" class="form-control"  value="{{ $item->amount }}" >
                                        </div>
                                    </td>
                                </tr>
                                @endforeach
                                <tr>
                                    <td>
                                        <div class="form-group">
                                            <button type="submit" class="btn-fill-lg bg-blue-dark btn-hover-yellow">{{__('Update')}}</button>
                                        </div>
                                    </td>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- Fee amount Edit Area End Here -->
@endsection