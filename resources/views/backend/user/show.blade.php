@extends('backend.master')
@section('content')
<!-- Breadcubs Area Start Here -->
<div class="breadcrumbs-area">
    {{-- <h3>Users</h3> --}}
    <ul>
        <li>
            <a href="{{route('dashboard')}}">{{__('Home')}}</a>
        </li>
        <li>{{__('User Details')}}</li>
    </ul>
</div>
<!-- Breadcubs Area End Here -->
<!-- Account Settings Area Start Here -->
<div class="row">
    <div class="col-12">
        <div class="card account-settings-box">
            <div class="card-body">
                <div class="heading-layout1">
                    <div class="item-title">
                        <h3>{{__('User Details')}}</h3>
                    </div>
                    <div class="pull-right">
                        <a href="{{route('user.index')}}"><button class="btn-fill-lg font-normal text-light gradient-orange-peel">{{__('All User')}}</button></a>
                    </div>
                </div>
                <hr><hr>
                <div class="user-details-box">
                    <div class="item-content">
                        <div class="info-table table-responsive">
                            <table class="table text-nowrap">
                                <tbody>
                                    <tr>
                                        <td>{{__('Username')}}</td>
                                        <td class="font-medium text-dark-medium">{{$user->username}}</td>
                                        <td>{{__('User Level:')}}</td>
                                        <td class="font-medium text-dark-medium">{{$user->user_level}}</td>
                                    </tr>
                                    <tr>
                                        <td>{{__('E-mail:')}}</td>
                                        <td class="font-medium text-dark-medium">{{$user->email}}</td>
                                        <td>{{__('Activation Status:')}}</td>
                                        <td class="font-medium text-dark-medium">
                                            @if ($user->status==1)
                                                {{_('Active User')}}
                                            @elseif($user->status==0)
                                             {{_('Inactive User')}}
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>{{__('Created By:')}}</td>
                                        <td class="font-medium text-dark-medium">{{$user->name}}</td>
                                        <td>{{__('Created At:')}}</td>
                                    <td class="font-medium text-dark-medium">{{ date('H:i A, dS M Y',strtotime($user->created_at)) }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection