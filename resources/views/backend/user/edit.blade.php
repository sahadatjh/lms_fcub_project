@extends('backend.master')
@section('content')
<!-- Breadcubs Area Start Here -->
<div class="breadcrumbs-area">
    {{-- <h3>Users</h3> --}}
    <ul>
        <li>
            <a href="{{route('dashboard')}}">{{__('Home')}}</a>
        </li>
        <li>{{__('Edit User')}}</li>
    </ul>
</div>
<!-- Breadcubs Area End Here -->
<!-- Account Settings Area Start Here -->
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="heading-layout1">
                    <div class="item-title">
                        <h3>{{ __('Update User Information')}}</h3>
                    </div>
                    <div class="pull-right">
                        <a href="{{route('user.index')}}"><button class="btn-fill-lg font-normal text-light gradient-orange-peel">All User</button></a>
                    </div>
                </div>
                <hr><hr>
                <form method="POST" action="{{ route('user.update',$user->id) }}" class="new-added-form">
                    @csrf
                    @if ($errors->any())
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    @endif
                    <div class="row">
                        <div class="col-xl-4 col-lg-6 col-12 form-group">
                            <label for="username">{{ __('Username *') }}</label>
                            <input id="username" type="text" name="username" class="form-control @error('username') is-invalid @enderror"  value="{{ $user->username }}"required>

                            @error('username')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="col-xl-4 col-lg-6 col-12 form-group">
                            <label for="name">{{ __('Full Name *') }}</label>
                            <input id="name" type="text" name="name" class="form-control @error('name') is-invalid @enderror"  value="{{ $user->name }}" required>
                        </div>
                        <div class="col-xl-4 col-lg-6 col-12 form-group">
                            <label for="email">{{ __('Your Email *') }}</label>
                            <input id="email" type="email"  name="email" class="form-control @error('email') is-invalid @enderror" value="{{ $user->email }}" placeholder="Ex. sahadat@gmail.com" autocomplete="email" required>

                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="col-xl-4 col-lg-6 col-12 form-group">
                            <label for="usertype">{{__('User Role *')}}</label>
                            <select name="usertype" id="usertype" class="select2 @error('usertype') is-invalid @enderror">
                                <option value="">Please Select*</option>
                                <option value="Super Admin" {{('Super Admin'==$user->role)?'selected':''}}>Super Admin</option>
                                <option value="Admin" {{('Admin'==$user->role)?'selected':''}}>Admin</option>
                                <option value="User" {{('User'==$user->role)?'selected':''}}>User</option>
                            </select>
                        </div>
                        <div class="col-xl-4 col-lg-6 col-12 form-group">
                            <label for="status">{{__('Activation Status *')}}</label>
                            <select name="status" id="status" class="select2  @error('status') is-invalid @enderror">
                                <option value="">Please Select *</option>
                                <option value="1" {{(1==$user->status)?'selected':''}}>{{__('Active')}}</option>
                                <option value="0" {{(0==$user->status)?'selected':''}}>{{__('Inactive')}}</option>
                            </select>
                        </div>
                        <div class="col-12 form-group mg-t-8">
                            <button type="submit" class="btn-fill-lg btn-gradient-yellow btn-hover-bluedark">Save</button>
                            <button type="reset" class="btn-fill-lg bg-blue-dark btn-hover-yellow">Reset</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection