@extends('backend.master')
@section('content')
<!-- Breadcubs Area Start Here -->
<div class="breadcrumbs-area">
    <ul>
        <li>
            <a href="{{route('dashboard')}}">{{__('Home')}}</a>
        </li>
        <li>{{__('All Fee Categories')}}</li>
    </ul>
</div>
<!-- Breadcubs Area End Here -->
<!-- Fee Category Area Start Here -->
<div class="card height-auto">
    <div class="card-body">
        <div class="heading-layout1">
            <div class="item-title">
                <h3>{{__('All Fee Categories')}}</h3>
            </div>
            <div class="pull-right">
                <a href="{{route('fee.category.create')}}"><button class="btn-fill-lg font-normal text-light gradient-orange-peel"> Add New</button></a>
            </div>
        </div>
        <hr><hr>
        <div class="table-responsive">
            <table class="table display data-table text-nowrap">
                <thead>
                    <tr>
                        <th>
                            <div class="form-check">
                                <input type="checkbox" class="form-check-input checkAll">
                                <label class="form-check-label">{{__('Sl No')}}</label>
                            </div>
                        </th>
                        <th>{{__('Category name')}}</th>
                        <th>{{__('Status')}}</th>
                        <th>{{__('Created By')}}</th>
                        <th>{{__('Action')}}</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($fee_categories as $key => $item)
                    <tr>
                        <td>
                            <div class="form-check">
                                <input type="checkbox" class="form-check-input">
                                <label class="form-check-label">{{$key+1}}</label>
                            </div>
                        </td>
                        <td>{{$item->category_name}}</td>
                        <td>{{$item->createdUser->username}}</td>
                        <td>
                            @if (1==$item->activation_status)
                                <span class="badge badge-pill badge-success">{{__('Active')}}</span>
                            @elseif(0==$item->activation_status)
                                <span class="badge badge-pill badge-warning">{{__('Inactive')}}</span>
                            @endif
                        </td>
                        <td>
                            <a href="{{route('fee.category.edit',$item->id)}}" class="btn btn-info" title="Edit"><i class="fas fa-edit"></i></a>
                            <a href="{{route('fee.category.destroy',$item->id)}}" id="delete"class="btn btn-danger" onclick="return check_delete();"title="Delete"><i class="fa fa-trash" aria-hidden="true"></i></a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<!-- Student Table Area End Here -->
@endsection