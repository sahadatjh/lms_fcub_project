@extends('layouts.main')

@section('content')

<div class="row">
    <div class="col-12">
        <div class="page-title-box">
                <h4 class="page-title"><i class="fa fa-file"></i> ADD FINISHING MACHINE</h4>
        </div>
    </div>
 </div>
 <div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                     <form class="form-horizontal" role="form" action="{{route('dyeingUtility.store')}}" method="POST">
                        {{ csrf_field() }}
                        <div class="row mt-3">
                            <div class="col-lg-12">
                                <table class="table table-bordered mb-0 text-center">
                                    <thead class="bg-primary text-white">
                                        <tr>
                                            <tr>
                                                <th>M/C No. </th>
                                                <th>M/C Type </th>
                                                <th>Brand </th>
                                                <th>Model </th>
                                                <th>Origin </th>
                                                <th>Capacity</th>
                                                <th>Year </th>
                                                <th>Action</th>
                                            </tr>
                                        </tr>
                                    </thead>
                                    <tbody class="tbody_order">
                                        <tr>
                                            <td>
                                                <input name="dyeing_utility_mc_no[]" type="text"  class="form-control text-center" placeholder="M/C NO" required>
                                            </td>
                                            <td>
                                                <input name="dyeing_utility_mc_type[]" type="text"  class="form-control text-center" placeholder="M/C NO" required>
                                            </td>
                                            <td>
                                                <input name="dyeing_utility_brand[]" type="text"  class="form-control text-center" placeholder="BRAND" required>
                                            </td>
                                            <td>
                                                <input name="dyeing_utility_model[]" type="text"  class="form-control text-center" placeholder="MODEL" required>
                                            </td><td>
                                                <input name="dyeing_utility_origin[]" type="text"  class="form-control text-center" placeholder="ORIGIN" required>
                                            </td>
                                            <td>
                                                <input name="dyeing_utility_capacity[]" type="text"  class="form-control text-center" placeholder="CAPACITY" required>
                                            </td>
                                            <td>
                                                <input name="dyeing_utility_year[]" type="text"  class="form-control text-center datepicker" placeholder="YEAR" required>
                                            </td>
                                                                                        
                                            <td>
                                                <a class="btn btn-danger btn-sm text-white edit remove_item_model" onclick="">
                                                    <i class="fa fa-trash"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    </tbody>

                                    <tfoot>
                                        <tr>
                                            <td class="text-x" colspan="13">

                                                <button type="button" class="add_item btn btn-sm btn-primary float-left"><i class="fa fa-plus"></i> ADD NEW ITEM</button>
                                              
                                                <button type="submit" value="submit" class="float-right btn btn btn-success"><i class="fa fa-save"></i> Submit</button>
                                            </td>
                                            
                                        </tr>
                                    </tfoot>
                                </table>
                            </div> <!-- end col -->
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
 </div>
 
    
@endsection

@section('plugin-styles')

<link href="{{ url('assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('plugin-scripts')

<script src="{{ url('assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
@endsection

@section('scripts')
<script>
    (function($){
        $(document).ready(function(){
            $(".datepicker").datepicker({
                todayHighlight  : true,
                format          : "yyyy",
                viewMode        : "years", 
                minViewMode     : "years",
                orientation     : 'bottom',
                autoclose       : true
            });
        });
    })(jQuery)
    var unique_id = 0;
            $(document).on('click', '.add_item', function(){
                var firstTr = $(document).find('.tbody_order tr');
                for(var i=0; i<firstTr.length; i++)
                {
                    if(firstTr[i])
                    {
                       var allInputs =[];
                       var inputs    =  $(document).find('.tbody_order tr input');
                       for(var i=0; i<inputs.length; i++){
                            allInputs.push(inputs[i]);
                       }
                        allInputs.pop();
                        for(var i=0; i<allInputs.length; i++)
                        {
                            if(allInputs[i].required )
                            {
                                if(allInputs[i].value == "")
                                {
                                    var field_name_arr  = allInputs[i].name.replace('[]','');
                                    var field_name      = field_name_arr.replace('_',' ');
                                    var field           = field_name.toUpperCase();
                                    alert(`Please Input ${field} field!`);
                                    return false;
                                }
                            }
                        }
                    }
                }
                unique_id+=1;

                let tr_id = new Date().getTime();
                //console.log("unique id",unique_id);
                $('.tbody_order').append(`
                    <tr data-tr-id="${tr_id}">
                        <td>
                            <input name="dyeing_utility_mc_no[]" type="text"  class="form-control text-center" placeholder="M/C NO" required>
                        </td>
                        <td>
                            <input name="dyeing_utility_mc_type[]" type="text"  class="form-control text-center" placeholder="M/C NO" required>
                        </td>
                        <td>
                            <input name="dyeing_utility_brand[]" type="text"  class="form-control text-center" placeholder="BRAND" required>
                        </td>
                        <td>
                            <input name="dyeing_utility_model[]" type="text"  class="form-control text-center" placeholder="MODEL" required>
                        </td>
                        <td>
                            <input name="dyeing_utility_origin[]" type="text"  class="form-control text-center" placeholder="ORIGIN" required>
                        </td>
                        <td>
                            <input name="dyeing_utility_capacity[]" type="text"  class="form-control text-center" placeholder="CAPACITY" required>
                        </td>
                        <td>
                            <input name="dyeing_utility_year[]" type="text"  class="form-control text-center datepicker" placeholder="YEAR" required>
                        </td>      
                        <td>
                            <a class="btn btn-danger btn-sm text-white edit remove_item_model" onclick="">
                                <i class="fa fa-trash"></i>
                            </a>
                        </td>
                                        
                    </tr>
                `);
                $(document).find(`[data-tr-id=${tr_id}] .datepicker`).datepicker({
                    todayHighlight  : true,
                    format          : "yyyy",
                    viewMode        : "years", 
                    minViewMode     : "years",
                    orientation     : 'bottom',
                    autoclose       : true
                });
                $(document).on('click', '.remove_item_model', function(){
                var rmTr = $(this).closest('tr').index();
                if(rmTr == 0)
                {
                    alert("You can't Delete First Item!");
                }else{
                    if(confirm('Are you sure?') )
                    $(this).closest('tr').remove();
                }
            });
        });   
</script>
    
@endsection