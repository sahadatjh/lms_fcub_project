<!doctype html>
<html class="no-js" lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Normalize CSS -->
    <link rel="stylesheet" href="{{asset('backend')}}/css/normalize.css">
    <!-- Main CSS -->
    <link rel="stylesheet" href="{{asset('backend')}}/css/main.css">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{asset('backend')}}/css/bootstrap.min.css">
    <!-- Fontawesome CSS -->
    <link rel="stylesheet" href="{{asset('backend')}}/css/all.min.css">
    <!-- Flaticon CSS -->
    <link rel="stylesheet" href="{{asset('backend')}}/fonts/flaticon.css">
    <!-- Animate CSS -->
    <link rel="stylesheet" href="{{asset('backend')}}/css/animate.min.css">
    <!-- Data Table CSS -->
    <link rel="stylesheet" href="{{asset('backend')}}/css/jquery.dataTables.min.css">
    <!-- Select 2 CSS -->
    <link rel="stylesheet" href="{{asset('backend')}}/css/select2.min.css">
    <!-- Date Picker CSS -->
    <link rel="stylesheet" href="{{asset('backend')}}/css/datepicker.min.css">
    {{-- Toster --}}
    <link rel="stylesheet" href="{{asset('backend')}}/css/toastr.min.css" />
    <!-- Custom CSS -->
    <link rel="stylesheet" href="{{asset('backend')}}/style.css">
    <!-- Modernize js -->
    <script src="{{asset('backend')}}/js/modernizr-3.6.0.min.js"></script>
</head>
<body>
    <!-- Preloader Start Here -->
    <div id="preloader"></div>
    <!-- Preloader End Here -->
    <!-- Login Page Start Here -->
    <div class="login-page-wrap">
        <div class="login-page-content">
            <div class="login-box">
                <div class="item-logo">
                    <img src="{{asset('backend')}}/img/logo2.png" alt="logo">
                </div>
                @yield('content')
            </div>
        </div>
    </div>
    <!-- Login Page End Here -->
    <!-- jquery-->
    <script src="{{asset('backend')}}/js/jquery-3.3.1.min.js"></script>
    <!-- Plugins js -->
    <script src="{{asset('backend')}}/js/plugins.js"></script>
    <!-- Popper js -->
    <script src="{{asset('backend')}}/js/popper.min.js"></script>
    <!-- Bootstrap js -->
    <script src="{{asset('backend')}}/js/bootstrap.min.js"></script>
    <!-- Scroll Up Js -->
    <script src="{{asset('backend')}}/js/jquery.scrollUp.min.js"></script>
    <!-- Full Calender Js -->
    <script src="{{asset('backend')}}/js/fullcalendar.min.js"></script>
    <!-- Chart Js -->
    <script src="{{asset('backend')}}/js/Chart.min.js"></script>
    <!-- Data Table Js -->
    <script src="{{asset('backend')}}/js/jquery.dataTables.min.js"></script>
    <!-- Select 2 Js -->
    <script src="{{asset('backend')}}/js/select2.min.js"></script>
    <!-- Date Picker Js -->
    <script src="{{asset('backend')}}/js/datepicker.min.js"></script>
    {{-- Toster --}}
    <script src="{{asset('backend')}}/js/toastr.min.js" ></script>
    {{-- Handlebars js --}}
    <script src="{{asset('backend')}}/js/handlebars.min.js" ></script>
    <!-- Custom Js -->
    <script src="{{asset('backend')}}/js/main.js"></script>
    
    {{--  Start Page script --}}
        @yield('script')
    {{--  End Page script --}}



</body>
</html>