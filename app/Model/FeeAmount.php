<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\Department;
use App\Model\Cariculam;
use App\Model\FeeCategory;

class FeeAmount extends Model
{
    public function cariculam(){
        return $this->belongsTo(Cariculam::class,'cariculam_id','id');
    }
    public function department(){
        return $this->belongsTo(Department::class,'department_id','id');
    }
    public function feeCategory(){
        return $this->belongsTo(FeeCategory::class,'fee_category_id','id');
    }
    public function createdUser(){
        return $this->belongsTo(User::class,'created_by','id');
    }
    public function updatedUser(){
        return $this->belongsTo(User::class,'updated_by','id');
    }
}
