<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class AssignSubject extends Model
{
    public function department(){
        return $this->belongsTo(Department::class,'department_id','id');
    }
    public function semester(){
        return $this->belongsTo(Semester::class,'semester_id','id');
    }
    public function subject(){
        return $this->belongsTo(Book::class,'subject_id','id');
    }
    public function createdUser(){
        return $this->belongsTo(User::class,'created_by','id');
    }
    public function updatedUser(){
        return $this->belongsTo(User::class,'updated_by','id');
    }
}
