<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\User;
class designation extends Model
{
    public function createdUser(){
        return $this->belongsTo(User::class,'created_by','id');
    }
}
