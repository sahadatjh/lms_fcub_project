<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Model\FeeCategory;

class FeeCategoryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $fee_categories=FeeCategory::all();
        return view('backend.fee_category.index',compact('fee_categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.fee_category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
	        'category_name' => 'required|string|unique:fee_categories|max:200|min:4',
	        'activation_status' => 'required|numeric',
        ]);

        $fee_category = new FeeCategory;
        $fee_category->category_name = $request->category_name;
        $fee_category->activation_status = $request->activation_status;
        $fee_category->created_by = Auth::user()->id;
        $saved= $fee_category->save();
        if ($saved) {
            return redirect()->route('fee.category.index')->with('success','data inserted successfully!');
        } else {
            return redirect()->route('fee.category.index')->with('error','Error!!! Please Check???');
        }
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $fee_category=FeeCategory::find($id);
        return view('backend.fee_category.edit',compact('fee_category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
	        'category_name' => 'required|string|max:200|min:4',
	        'activation_status' => 'required|numeric',
        ]);

        $fee_category = FeeCategory::find($id);
        $fee_category->category_name = $request->category_name;
        $fee_category->activation_status = $request->activation_status;
        $fee_category->updated_by = Auth::user()->id;
        $saved= $fee_category->save();
        if ($saved) {
            return redirect()->route('fee.category.index')->with('success','data Updated successfully!');
        } else {
            return redirect()->route('fee.category.index')->with('error','Error!!! Please Check???');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $fee_category=FeeCategory::find($id);
        $fee_category->delete();
        return redirect()->route('fee.category.index')->with('success','data deleted successfully!');
    }
}
