<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Model\AssignStudent;
use App\Model\DiscountStudent;
use App\Model\Department;
use App\Model\Semester;
use App\Model\Session;
use App\Model\User;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use DB;

class AssignStudentController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $students=AssignStudent::all();
        return view('backend.student.index',['students'=>$students]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $departments=Department::where('activation_status',1)->get();
        $semesters=Semester::where('activation_status',1)->get();
        $sessions=Session::where('activation_status',1)->get();
        return view('backend.student.create',compact('departments','semesters','sessions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::transaction(function() use($request){
            $request->validate([
                'name' => 'required|string|max:200',
                'fname' => 'required|string|max:200',
                'mname' => 'required|string|max:200',
                'college_roll' => 'required|numeric|unique:assign_students',
                'board_roll' => 'required|numeric|unique:assign_students',
                'registration' => 'required|numeric|unique:assign_students',
                'phone' => 'required|numeric',
                'religion' => 'required',
                'gender' => 'required|max:10',
                'department_id' => 'required',
                'semester_id' => 'required',
                'session_id' => 'required',
                'nationality' => 'required',
                'address' => 'required|max:250',
                'activation_status' => 'required|numeric',
            ]);
            
            $user = new User();
            $user->name = $request->name;
            $user->mobile = $request->phone;
            $user->email = $request->email;
            $code= uniqid();
            $user->code = $code;
            $user->password = Hash::make($code);
            $user->gender = $request->gender;
            $user->religion = $request->religion;
            $user->address = $request->address;
            $user->created_by = Auth::user()->id;
            $user->dob = $request->dob;
            
            $image=$request->file('image');
            if ($image) {
                $image_name=Str::random(10);
                $ext=strtolower($image->getClientOriginalExtension());
                $image_full_name=$image_name.'.'.$ext;
                $upload_path='public/backend/uploads/';
                $image_url=$upload_path.$image_full_name;
                $success=$image->move($upload_path,$image_full_name);
                $user->image=$image_url;
            }
            $user->save();


            $assignStudent = new AssignStudent();
            $assignStudent->student_id=$user->id;
            $assignStudent->college_roll = $request->college_roll;
            $assignStudent->board_roll = $request->board_roll;
            $assignStudent->registration = $request->registration;
            $assignStudent->fname = $request->fname;
            $assignStudent->mname = $request->mname;
            $assignStudent->gardian_phone = $request->gardian_phone;
            $assignStudent->gardian_profession = $request->gardian_profession;
            $assignStudent->blood_group = $request->blood_group;
            $assignStudent->nationality = $request->nationality;
            $assignStudent->admission_date = $request->admission_date;
            $assignStudent->department_id = $request->department_id;
            $assignStudent->semester_id = $request->semester_id;
            $assignStudent->session_id = $request->session_id;
            $assignStudent->semester_fee = $request->semester_fee;
            $assignStudent->total_fee = $request->total_fee;
            $assignStudent->remarks = $request->remarks;
            $assignStudent->activation_status = $request->activation_status;
            $assignStudent->save();
            
            $discountStudent = new DiscountStudent();
            $discountStudent->assign_student_id = $assignStudent->id;
            $discountStudent->discount = $request->discount;
            $discountStudent->save();
        });
        return redirect()->route('student.index')->with('success','Student Admission successfully!');
        
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $student=AssignStudent::find($id);
        return view('backend.student.show',compact('student'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $student=AssignStudent::find($id);
        $departments = Department::all();
        $semesters = Semester::all();
        $sessions = Session::all();
        return view('backend.student.edit',compact('student','departments','semesters','sessions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->id=$id;
        DB::transaction(function() use($request){
            // dd($this->id);
            $request->validate([
                'name' => 'required|string|max:200',
                'fname' => 'required|string|max:200',
                'mname' => 'required|string|max:200',
                'college_roll' => "required|numeric|unique:assign_students,college_roll,{$this->id}",
                'board_roll' => "required|numeric|unique:assign_students,board_roll,{$this->id}",
                'registration' =>"required|numeric|unique:assign_students,registration,{$this->id}",
                'phone' => 'required|numeric',
                'religion' => 'required',
                'gender' => 'required|max:10',
                'department_id' => 'required',
                'semester_id' => 'required',
                'session_id' => 'required',
                'nationality' => 'required',
                'address' => 'required|max:250',
                'activation_status' => 'required|numeric',
            ]);
            $assignStudent =  AssignStudent::where('id',$this->id)->first();
            // dd($assignStudent);
            $assignStudent->student_id=$request->student_id;
            $assignStudent->college_roll = $request->college_roll;
            $assignStudent->board_roll = $request->board_roll;
            $assignStudent->registration = $request->registration;
            $assignStudent->fname = $request->fname;
            $assignStudent->mname = $request->mname;
            $assignStudent->gardian_phone = $request->gardian_phone;
            $assignStudent->gardian_profession = $request->gardian_profession;
            $assignStudent->blood_group = $request->blood_group;
            $assignStudent->nationality = $request->nationality;
            $assignStudent->admission_date = $request->admission_date;
            $assignStudent->department_id = $request->department_id;
            $assignStudent->semester_id = $request->semester_id;
            $assignStudent->session_id = $request->session_id;
            $assignStudent->semester_fee = $request->semester_fee;
            $assignStudent->total_fee = $request->total_fee;
            $assignStudent->remarks = $request->remarks;
            $assignStudent->activation_status = $request->activation_status;
            $assignStudent->save();
            
            $user = User::find($request->student_id);
            $user->name = $request->name;
            $user->mobile = $request->phone;
            $user->email = $request->email;
            $user->gender = $request->gender;
            $user->religion = $request->religion;
            $user->address = $request->address;
            $user->dob = $request->dob;

            $image=$request->file('image');
            if ($image) {
                $image_name=Str::random(10);
                $ext=strtolower($image->getClientOriginalExtension());
                $image_full_name=$image_name.'.'.$ext;
                $upload_path='public/backend/uploads/';
                $image_url=$upload_path.$image_full_name;
                if (file_exists($user->image)) {
                    unlink($user->image);
                }
                $success=$image->move($upload_path,$image_full_name);
                $user->image=$image_url;
            }
            $user->save();


            
            $discountStudent = DiscountStudent::where('assign_student_id',$this->id)->first();
            $discountStudent->discount = $request->discount;
            $discountStudent->save();
        });
        return redirect()->route('student.index')->with('success','Inormation updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
        DB::transaction(function() use($id){
            $student = AssignStudent::find($id);
            $student_user = User::find($student->student_id);
            $discountStudent = DiscountStudent::where('assign_student_id',$id)->first();
            // dd($discountStudent);

            $student->delete();
            $student_user->delete();
            $discountStudent->delete();
        });
        return redirect()->route('student.index')->with('success','Student deleted  successfully!');

    }
}
