<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Model\FeeAmount;
use App\Model\Department;
use App\Model\FeeCategory;
use App\Model\Cariculam;

class FeeAmountController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $fee_amount=FeeAmount::select('department_id','cariculam_id','created_by')->groupBy('department_id','cariculam_id','created_by')->get();
        return view('backend.fee_amount.index',compact('fee_amount'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $departments=Department::where('activation_status',1)->get();
        $categories=FeeCategory::where('activation_status',1)->get();
        $cariculams=Cariculam::where('activation_status',1)->get();
        return view('backend.fee_amount.create',compact('departments','categories','cariculams'));
    }
    public function show($id){

        $department=Department::where('id',$id)->first();
        $fee_amount=FeeAmount::where('department_id',$id)->get();
        return view('backend.fee_amount.show',compact('fee_amount','department'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        $request->validate([
	        'cariculam_id' => 'required|numeric',
	        'department_id' => 'required|numeric',
        ]);

        $count_row = count($request->fee_category_id);
        for ($i=0; $i < $count_row; $i++) { 
            $fee_amount = new FeeAmount();
            $fee_amount->cariculam_id=$request->cariculam_id;
            $fee_amount->department_id=$request->department_id;
            $fee_amount->fee_category_id=$request->fee_category_id[$i];
            $fee_amount->amount=$request->amount[$i];
            $fee_amount->created_by=Auth::user()->id;
            $saved= $fee_amount->save();
        }
        if ($saved) {
            return redirect()->route('fee.amount.index')->with('success','data inserted successfully!');
        } else {
            return redirect()->route('fee.amount.create')->with('error','Error!!! Please Check???');
        }
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($department_id)
    {
        $department=Department::where('id',$department_id)->first();
        $fee_amount=FeeAmount::where('department_id',$department_id)->get();
        return view('backend.fee_amount.edit',compact('fee_amount','department'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $department_id)
    {
        
        $request->validate([
	        'fee_category_id' => 'required',
	        'amount' => 'required',
        ]);
        $fee_amount = FeeAmount::where('department_id',$department_id)->delete();
        $count_row = count($request->fee_category_id);
        for ($i=0; $i < $count_row; $i++) { 
            $fee_amount = new FeeAmount();
            $fee_amount->cariculam_id=$request->cariculam_id;
            $fee_amount->department_id=$request->department_id;
            $fee_amount->fee_category_id=$request->fee_category_id[$i];
            $fee_amount->amount=$request->amount[$i];
            $fee_amount->created_by=Auth::user()->id;
            $saved= $fee_amount->save();
        }
        if ($saved) {
            return redirect()->route('fee.amount.index')->with('success','data Updated successfully!');
        } else {
            return redirect()->route('fee.amount.create')->with('error','Error!!! Please Check???');
        }
    }

}
