<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Model\ExamType;

class ExamTypeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $exam_types=ExamType::all();
        return view('backend.exam_type.index',compact('exam_types'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.exam_type.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
	        'exam_type_name' => 'required|string|unique:exam_types|max:200|min:4',
	        'activation_status' => 'required|numeric',
        ]);

        $exam_type = new ExamType;
        $exam_type->exam_type_name = $request->exam_type_name;
        $exam_type->activation_status = $request->activation_status;
        $exam_type->created_by = Auth::user()->id;
        $saved= $exam_type->save();
        if ($saved) {
            return redirect()->route('exam.type.index')->with('success','data inserted successfully!');
        } else {
            return redirect()->route('exam.type.index')->with('error','Error!!! Please Check???');
        }
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $exam_type=ExamType::find($id);
        return view('backend.exam_type.edit',compact('exam_type'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'activation_status' => 'required|numeric',
	        'exam_type_name' => 'required|string|max:200|min:4|unique:exam_types,exam_type_name,' . $id ,
        ]);

        $exam_type = ExamType::find($id);
        $exam_type->exam_type_name = $request->exam_type_name;
        $exam_type->activation_status = $request->activation_status;
        $exam_type->updated_by = Auth::user()->id;
        $saved= $exam_type->save();
        if ($saved) {
            return redirect()->route('exam.type.index')->with('success','data Updated successfully!');
        } else {
            return redirect()->route('exam.type.index')->with('error','Error!!! Please Check???');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $exam_type=ExamType::find($id)->delete();
        return redirect()->route('exam.type.index')->with('success','data deleted successfully!');
    }
}
