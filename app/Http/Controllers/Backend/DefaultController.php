<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\AssignStudent;
use App\Model\Department;
use App\Model\Semester;
use App\Model\Purchase;

class DefaultController extends Controller
{
    public function getStudent(Request $request)
    {
        if ($request->ajax()) { 
            $output = "";
            $student = AssignStudent::where('board_roll',$request->roll)->first();
            if($student==null){
                $output.= "Roll ".$request->roll." not found!!! Please search again!!!";
                return $output;
            }
            $department=Department::where('id',$student->department_id)->first();
            $semester=Semester::where('id',$student->semester_id)->first(); 
            
            if ($student!=null) {
            $output .='<div class="info-table table-responsive">
                        <table class="table text-nowrap">
                            <tbody>
                                <tr>
                                    <th>Name:</th>
                                    <td id="name" class="font-medium">'.$student->get_student_user->name.'</td>
                                    <th>Phone:</th>
                                    <td id="phone" class="font-medium">'.$student->get_student_user->mobile.'</td>
                                    <td id="phone" class="font-medium" rowspan="4">
                                        <img src="'.asset($student->get_student_user->image).'" alt="student" style="height: 200px; width: 160px; border: 1px solid #000;padding: 5px">
                                    </td>
                                </tr>
                                <tr>
                                    <th>Board Roll:</th>
                                    <td id="roll" class="font-medium">'.$student->board_roll.'</td>
                                    <th>Registration:</th>
                                    <td id="registration" class="font-medium">'.$student->registration.'</td>
                                </tr>
                                <tr>
                                    <th>Father Name:</th>
                                    <td id="fname" class="font-medium">'.$student->fname.'</td>
                                    <th>Mother name:</th>
                                    <td id="mname" class="font-medium">'.$student->mname.'</td>
                                </tr>
                                <tr>
                                    <th>Department:</th>
                                    <td id="department" class="font-medium">'.$department->name.'</td>
                                    <th>Semester:</th>
                                    <td id="semester" class="font-medium">'.$semester->name.'</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>';
            }
            return $output;
        }
    }

    public function getPublication(Request $request){
        $book_id = $request->book_id;
        $publications=Purchase::with(['name'])->select('publication_id')->where('book_id',$book_id)->groupBy('publication_id')->get();
        return response()->json($publications);
    }
    public function getAuthor(Request $request){
        $publication_id = $request->publication_id;
        $authors=Purchase::with(['authorName'])->select('author_id')->where('publication_id',$publication_id)->groupBy('author_id')->get();
        // dd($authors->toArray());
        return response()->json($authors);
    }
}
