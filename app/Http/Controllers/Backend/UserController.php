<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use App\User;
use DB;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users= User::all();
        return view('backend.user.index',compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.user.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
	        'username' => 'required|string|max:200|min:4','without_spaces','unique:users',
	        'name' => 'required|string|max:200|min:4',
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
	        'usertype' => 'required',
	        'status' => 'required',
	        'password' => ['required', 'string', 'min:4', 'confirmed'],
        ]);

        $user = new User;
        $user->username = $request->username;
        $user->email = $request->email;
        $user->name = $request->name;
        $user->role = $request->usertype;
        $user->code = $request->password;
        $user->password = Hash::make($request->password);
        $user->status = $request->status;
        $user->created_by = Auth::user()->id;
        $user->save();
        return redirect()->route('user.index')->with('success','data inserted successfully!!!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user=User::find($id);
        return view('backend.user.show',['user'=>$user]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user=User::find($id);
        return view('backend.user.edit',compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $request->validate([
	        'username' => 'required|string|max:200|min:4',
            'email' => 'required|string|email|max:255',
	        'usertype' => 'required',
	        'status' => 'required',
        ]);
        $user = User::find($id);
        $user->username = $request->username;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->role = $request->usertype;
        $user->status = $request->status;
        $user->save();
        return redirect()->route('user.index')->with('success','User Updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user=User::find($id);
        $user->delete();
		return redirect()->route('user.index')->with('success','data deleted successfully!!!');
    }


    public function password(){
        return view('backend.user.password');
    }
    public function passwordUpdate(Request $request){
        $request->validate([
	        'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
        $id=Auth::user()->id;
        $user=User::find($id);
        $user->password=Hash::make($request->password);
        $user->save();
        return redirect()->route('user.index')->with('success','password changed successfully!!!');
        
    }
}
