<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Model\Designation;

class DesignationController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $designations=Designation::all();
        return view('backend.designation.index',compact('designations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.designation.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
	        'title' => 'required|string|unique:designations|max:200|min:4',
	        'activation_status' => 'required|numeric',
        ]);

        $designation = new Designation;
        $designation->title = $request->title;
        $designation->activation_status = $request->activation_status;
        $designation->created_by = Auth::user()->id;
        $saved= $designation->save();
        if ($saved) {
            return redirect()->route('designation.index')->with('success','data inserted successfully!');
        } else {
            return redirect()->route('designation.index')->with('error','Error!!! Please Check???');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $designation=Designation::find($id);
        return view('backend.designation.edit',compact('designation'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
	        'title' => 'required|string|max:200|min:4',
	        'activation_status' => 'required|numeric',
        ]);

        $designation = Designation::find($id);
        $designation->title = $request->title;
        $designation->activation_status = $request->activation_status;
        $designation->updated_by = Auth::user()->id;
        $saved= $designation->save();
        if ($saved) {
            return redirect()->route('designation.index')->with('success','data Updated successfully!');
        } else {
            return redirect()->route('designation.index')->with('error','Error!!! Please Check???');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $designation=Designation::find($id);
        $designation->delete();
        return redirect()->route('designation.index')->with('success','data deleted successfully!');
    }
}
