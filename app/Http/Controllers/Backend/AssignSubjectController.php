<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Model\AssignSubject;
use App\Model\Department;
use App\Model\Semester;
use App\Model\Book;

class AssignSubjectController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $assign_subjects=AssignSubject::select('department_id','semester_id')->groupBy('department_id','semester_id')->get();
        return view('backend.assign_subject.index',compact('assign_subjects'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $departments=Department::where('activation_status',1)->get();
        $semesters=Semester::where('activation_status',1)->get();
        $subjects=Book::where('activation_status',1)->get();
        return view('backend.assign_subject.create',compact('departments','semesters','subjects'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
	        'department_id' => 'required|numeric',
	        'semester_id' => 'required|numeric',
	        'subject_id' => 'required|numeric',
	        // 'tc_mark' => 'required|numeric',
	        // 'tc_pass_mark' => 'required|numeric',
	        // 'tf_mark' => 'required|numeric',
	        // 'tf_pass_mark' => 'required|numeric',
	        // 'pc_mark' => 'required|numeric',
	        // 'pc_pass_mark' => 'required|numeric',
	        // 'pf_mark' => 'required|numeric',
	        // 'pf_pass_mark' => 'required|numeric',
	        // 'total_mark' => 'required|numeric',
	        // 'total_pass_mark' => 'required|numeric',
        ]);
        $assign_subject = new AssignSubject;
        $assign_subject->department_id = $request->department_id;
        $assign_subject->semester_id = $request->semester_id;
        $assign_subject->subject_id = $request->subject_id;
        $assign_subject->tc_mark = $request->tc_mark;
        $assign_subject->tc_pass_mark = $request->tc_pass_mark;
        $assign_subject->tf_mark = $request->tf_mark;
        $assign_subject->tf_pass_mark = $request->tf_pass_mark;
        $assign_subject->pc_mark = $request->pc_mark;
        $assign_subject->pc_pass_mark = $request->pc_pass_mark;
        $assign_subject->pf_mark = $request->pf_mark;
        $assign_subject->pf_pass_mark = $request->pf_pass_mark;
        $assign_subject->total_mark = $request->total_mark;
        $assign_subject->total_pass_mark = $request->total_pass_mark;
        $assign_subject->created_by = Auth::user()->id;
        $saved= $assign_subject->save();
        if ($saved) {
            return redirect()->route('assign.subject.create')->with('success','data inserted successfully!');
        } else {
            return redirect()->route('assign.subject.create')->with('error','Error!!! Please Check???');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($department_id,$semester_id)
    {
        $assign_subject_by_semester=AssignSubject::where('department_id',$department_id)->where('semester_id',$semester_id)->get();
        $department=Department::select('name')->where('id',$department_id)->first();
        $semester=Semester::select('name')->where('id',$semester_id)->first();
        return view('backend.assign_subject.show',compact('assign_subject_by_semester','department','semester'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $assign_subject=AssignSubject::find($id);
        $departments=Department::where('activation_status',1)->get();
        $semesters=Semester::where('activation_status',1)->get();
        $subjects=Book::where('activation_status',1)->get();
        return view('backend.assign_subject.edit',compact('departments','semesters','subjects','assign_subject'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
	        'department_id' => 'required|numeric',
	        'semester_id' => 'required|numeric',
	        'subject_id' => 'required|numeric',
	        // 'tc_mark' => 'required|numeric',
	        // 'tc_pass_mark' => 'required|numeric',
	        // 'tf_mark' => 'required|numeric',
	        // 'tf_pass_mark' => 'required|numeric',
	        // 'pc_mark' => 'required|numeric',
	        // 'pc_pass_mark' => 'required|numeric',
	        // 'pf_mark' => 'required|numeric',
	        // 'pf_pass_mark' => 'required|numeric',
	        // 'total_mark' => 'required|numeric',
	        // 'total_pass_mark' => 'required|numeric',
        ]);
        $assign_subject=AssignSubject::find($id);
        $assign_subject->department_id = $request->department_id;
        $assign_subject->semester_id = $request->semester_id;
        $assign_subject->subject_id = $request->subject_id;
        $assign_subject->tc_mark = $request->tc_mark;
        $assign_subject->tc_pass_mark = $request->tc_pass_mark;
        $assign_subject->tf_mark = $request->tf_mark;
        $assign_subject->tf_pass_mark = $request->tf_pass_mark;
        $assign_subject->pc_mark = $request->pc_mark;
        $assign_subject->pc_pass_mark = $request->pc_pass_mark;
        $assign_subject->pf_mark = $request->pf_mark;
        $assign_subject->pf_pass_mark = $request->pf_pass_mark;
        $assign_subject->total_mark = $request->total_mark;
        $assign_subject->total_pass_mark = $request->total_pass_mark;
        $assign_subject->updated_by = Auth::user()->id;
        $saved= $assign_subject->save();
        if ($saved) {
            return redirect()->route('assign.subject.index')->with('success','data updated successfully!');
        } else {
            return redirect()->route('assign.subject.index')->with('error','Error!!! Please Check???');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $assign_subject=AssignSubject::find($id);
        $deleted=$assign_subject->delete();
        if ($deleted) {
            return redirect()->route('assign.subject.index')->with('success','data deleted successfully!');
        } else {
            return redirect()->route('assign.subject.index')->with('error','Error!!! Please Check???');
        }
    }
}
