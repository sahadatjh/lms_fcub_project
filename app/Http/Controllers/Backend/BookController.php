<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Model\User;
use App\Model\Book;

class BookController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $books=Book::all();
        return view('backend.book.index',compact('books'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.book.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
	        'code' => 'required|numeric|unique:books|max:99999',
            'name' => 'required|string|unique:books|max:200|min:4',
        ]);
        $book = new Book;
        $book->code = $request->code;
        $book->name = $request->name;
        $book->theory = $request->theory;
        $book->practical = $request->practical;
        $book->credit = $request->credit;
        $book->activation_status = $request->activation_status;
        $book->created_by = Auth::user()->id;
        $saved= $book->save();
        if ($saved) {
            return redirect()->route('book.index')->with('success','data inserted successfully!');
        } else {
            return redirect()->route('book.index')->with('error','Error!!! Please Check???');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       $book=Book::find($id);
        return view('backend.book.edit',compact('book'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required|string|max:200|unique:books,name,' . $id ,
	        'code' => 'required|numeric|unique:books,code,' . $id,
        ]);
        $book = Book::find($id);
        $book->code = $request->code;
        $book->name = $request->name;
        $book->theory = $request->theory;
        $book->practical = $request->practical;
        $book->credit = $request->credit;
        $book->activation_status = $request->activation_status;
        $book->updated_by = Auth::user()->id;
        $saved= $book->save();
        if ($saved) {
            return redirect()->route('book.index')->with('success','data updaded successfully!');
        } else {
            return redirect()->route('book.index')->with('error','Error!!! Please Check???');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $book=Book::find($id)->delete();
        if ($book) {
            return redirect()->route('book.index')->with('success','data deleted successfully!');
        } else {
            return redirect()->route('book.index')->with('error','Error!!! Please Check???');
        }
    }

}
