<?php

namespace App\Http\Controllers;

use App\Model\AssignStudent;
use App\Model\Book;
use App\Model\Purchase;
use App\Model\Student;
use Illuminate\Http\Request;

class loginController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    //show admin dashboard page after login
    public function dashboard()
    {
        $total_student = AssignStudent::all()->count();
        $total_books = Purchase::sum('quantity');
        $in_stock = Book::sum('quantity');
        return view('backend.admin_dashboard', compact('total_student', 'total_books', 'in_stock'));
    }
}
