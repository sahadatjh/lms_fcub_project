<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssignStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assign_students', function (Blueprint $table) {
            $table->id();
            $table->integer('student_id')->comment('user_id=student_id');
            $table->integer('cariculam_id')->nullable();
            $table->integer('department_id')->nullable();
            $table->integer('semester_id')->nullable();
            $table->integer('session_id')->nullable();
            $table->integer('shift_id')->nullable();
            $table->integer('college_roll')->nullable();
            $table->integer('board_roll')->nullable();
            $table->integer('registration')->nullable();
            $table->string('fname')->nullable();
            $table->string('mname')->nullable();
            $table->string('gardian_profession')->nullable();
            $table->string('gardian_phone')->nullable();
            $table->string('blood_group')->nullable();
            $table->string('nationality')->nullable();
            $table->date('admission_date')->nullable();
            $table->float('semester_fee')->nullable();
            $table->float('total_fee')->nullable();
            $table->boolean('activation_status')->default(0);
            $table->string('remarks')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assign_students');
    }
}
