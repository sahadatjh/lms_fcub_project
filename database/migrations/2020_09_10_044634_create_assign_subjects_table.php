<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssignSubjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assign_subjects', function (Blueprint $table) {
            $table->id();
            $table->integer('subject_id');
            $table->integer('department_id');
            $table->integer('semester_id');
            $table->double('tc_mark')->nullable();
            $table->double('tc_pass_mark')->nullable();
            $table->double('tf_mark')->nullable();
            $table->double('tf_pass_mark')->nullable();
            $table->double('pc_mark')->nullable();
            $table->double('pc_pass_mark')->nullable();
            $table->double('pf_mark')->nullable();
            $table->double('pf_pass_mark')->nullable();
            $table->double('total_mark')->nullable();
            $table->double('total_pass_mark')->nullable();
            $table->tinyInteger('activation_status')->default('1');
            $table->tinyInteger('created_by');
            $table->tinyInteger('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assign_subjects');
    }
}
